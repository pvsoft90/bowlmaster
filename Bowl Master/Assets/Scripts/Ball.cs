﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

	public Vector3 launchVelocity;
	private Rigidbody rigidbody;
    private AudioSource audioSource;
	public bool isPlaying = false;

	private Vector3 ballStartPos;
	private Rigidbody myRigidbody;

	// Use this for initialization
	void Start () {
        rigidbody = GetComponent<Rigidbody>();
		rigidbody.useGravity = false;

		ballStartPos = gameObject.transform.position;	// vị trí đầu tiên của Ball trước khi được ném đi
	}

	// Ném Ball : được gọi trong hàm DrapEnd (khi release mouse)
	public void Launch(Vector3 velocity) {
		isPlaying = true;
		rigidbody.useGravity = true;
		audioSource = GetComponent<AudioSource>();
        audioSource.Play();		
		rigidbody.velocity = - velocity;	// Vị trí mặc định của Z=0, khi Ball lăn đến Pin thì giá trị nhỏ dần , nếu không để dấu âm thì Ball sẽ chạy ngược 
    }

	// Reset lại vị trí của Ball
	public void ResetBallPosition(){
		isPlaying = false;
		gameObject.transform.position = ballStartPos;	// gán lại vị trí Ball giống lúc ban 
		rigidbody.velocity = Vector3.zero;				// vận tốc bằng 0
		rigidbody.angularVelocity = Vector3.zero;	
	}
}
