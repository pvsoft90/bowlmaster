﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Ball))]
public class BallDrapLaunch : MonoBehaviour {

	private Vector3 dragStart, dragEnd;
	private float startTime, endTime;
	private Ball ball;
	// Use this for initialization
	void Start () {
		ball = GetComponent<Ball> ();
	}
	
	public void DrapStart(){
		dragStart = Input.mousePosition;	// Vị trí của Ball khi bắt đầu click mouse
		startTime = Time.time;
	}

	public void MoveStart(float amount){
		if (!ball.isPlaying) {
			ball.transform.Translate (new Vector3 (amount, 0, 0));
		}
	}

	public void DrapEnd(){
		dragEnd = Input.mousePosition;		// Vị trí của Ball khi release mouse 
		endTime = Time.time;

		float dragDuration = endTime - startTime;

		float launchSpeedX = (dragEnd.x - dragStart.x) / dragDuration; 
		float launchSpeedZ = (dragEnd.y - dragStart.y) / dragDuration;
		Vector3 launchVelocity = new Vector3 (launchSpeedX, 0, launchSpeedZ);
		ball.Launch(launchVelocity);
	}
}
