﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour {

	public Ball ball;

	private Vector3 offset;	// Khoảng cách giữa Ball và Camera

	// Use this for initialization
	void Start () {
		offset = transform.position - ball.transform.position;	// khoảng cách giữa Ball và Camera trước khi Ball được ném
	}
	
	// Update is called once per frame
	void Update () {
		// Nếu vị trí của Ball mà lớn hơn -1729 ( Ball vẫn chưa lao ra khỏi chiều sâu của Sàn) 
		if (ball.transform.position.z >= -1829) {
			transform.position = ball.transform.position + offset;	// vị trí hiện tại của Camera = vị trí Ball + khoảng cách lúc đầu giữa Ball và  
		}
	}
}
