﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pin : MonoBehaviour {


	public float standingTreshold = 3f;

	private float distanceOfRasing = 40f;	// khoảng cách so với sàn khi dựng đặt lại Pin

	// Kiểm tra xem từng Pin có bị ngã 
	public bool IsStanding() {
		Vector3 rotationInEuler = transform.rotation.eulerAngles;

		float tiltInX = Mathf.Abs (270 - rotationInEuler.x);	//X: chiều ngang ; nếu 270 không bị trừ thì mặc định Pin nằm ngang (bị do lúc design )
		float tiltInZ = Mathf.Abs (rotationInEuler.z);	// Z : chiều dài của sàn 

		if (tiltInX < standingTreshold || tiltInZ < standingTreshold) { // nếu độ nghiên của Pin nhỏ hơn độ nghiên quy định thì bật cờ true
			return true;
		} else {
			return false;
		}
	}

	// Dựng Pin lên trong trường hợp không bị ngã
	public void RaisIfStanding() {
		if(IsStanding()) {
			gameObject.GetComponent<Rigidbody> ().useGravity = false;	// nếu có gravity lúc này nó sẽ bị rớt xuống và có thể bị ngã 
			transform.Translate (new Vector3 (0, distanceOfRasing, 0), Space.World);	
		}
	}

	public void Lower(){
		transform.Translate (new Vector3 (0, - distanceOfRasing, 0), Space.World);	// gắn dấu âm là để Pin nằm sát mặt sàn (có liên quan đến lúc thiết kế giao )
		gameObject.GetComponent<Rigidbody> ().useGravity = true;
	}
}
