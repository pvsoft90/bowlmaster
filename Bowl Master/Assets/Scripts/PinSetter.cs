﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PinSetter : MonoBehaviour {

	public int lastStandingCount = -1;
	private float lastChangeTime;

	private Ball ball;
	public Text displayScore;
	private bool ballEnterBox = false;

	public GameObject pinSet;

	// Use this for initialization
	void Start () {
		ball = GameObject.FindObjectOfType<Ball> ();
	}
	
	// Update is called once per frame
	void Update () {
		displayScore.text = CountStanding ().ToString ();	// Show số lượng Ball còn đứng vững 

		if(ballEnterBox) { // nếu Ball đã lao vào Box Collider của Pin Setter 
			UpdateStandingCountAndSettle ();
		}
	}

	// Đếm xem có bao nhiêu Ball còn đứng vững 
	int CountStanding() {
		int standing = 0;
		foreach (Pin pin in GameObject.FindObjectsOfType<Pin>()) { // duyệt hết các Pin object 
			if (pin.IsStanding ()) {
				standing++;
			}
		}

		return standing;
	}

	// Box Collider của Pin Setter hứng va chạm khi Ball lao vào 
	void OnTriggerEnter(Collider collider){
		GameObject thingHit = collider.gameObject;	
		if (thingHit.GetComponent<Ball> ()) {	// Nếu collider này có chứa thành phần script <Ball> thì nó chính là Ball 
			ballEnterBox = true;
			displayScore.color = Color.red;	// đổi màu của Text 
		}
	}

	// Được gọi trong event của animation Raise
	public void RaisePins() {
		foreach (Pin pin in GameObject.FindObjectsOfType<Pin>()) {
			pin.RaisIfStanding ();
		}
	}
		
	public void LowerPins(){
		foreach (Pin pin in GameObject.FindObjectsOfType<Pin>()) {
			pin.Lower ();;
		}
	}

	//Được gọi trong event của animation Renew  
	public void RenewPins() {
		GameObject newPins = Instantiate (pinSet);
		newPins.transform.position += new Vector3 (0, 15, 0);
	}

	void UpdateStandingCountAndSettle() {
		int currentStanding = CountStanding();
		if (currentStanding != lastStandingCount) {
			lastChangeTime = Time.time;
			lastStandingCount = currentStanding;
			return;
		}

		float settleTime = 3f;
		if ((Time.time - lastChangeTime) > settleTime) {
			//if last change > 3s ago
			PinHaveSettled();
		}
			
	}

	void PinHaveSettled() {
		lastStandingCount = -1;	
		ballEnterBox = false;
		displayScore.color = Color.green;
		ball.ResetBallPosition ();
	}
}
