﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Hàm này dùng để huỷ các Pin khi rơi ra khỏi Box Collider
public class Shreadder : MonoBehaviour {

	void OnTriggerExit(Collider collider){
		GameObject thingLeft = collider.gameObject;
		if (thingLeft.GetComponent<Pin> ()) {
			DestroyObject (thingLeft);
		}
	}
}

